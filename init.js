var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();

var Client = require('node-rest-client').Client;

app.get('/scrape', function(req, res) {

    var getPage = function(url) {
        return new Promise(function(resolve, reject) {
            request(url, function(error, response, html) {

                var json = { title: "", p: "" };

                if (!error) {
                    var $ = cheerio.load(html);
                }

                $('h1').filter(function() {
                    var data = $(this);
                    let text = data.text();
                    json.title = text;
                    resolve(json);
                });

                $('p').filter(function() {
                    var data = $(this);
                    let text = data.text();
                    json.p = text;
                    resolve(json);
                });
            });
        });
    }

    var sendAPI = function(url) {
        var post = { Host: url, Dados: [] };

        getPage(url).then(function(json) {
            if (json.title != '' && json.p != '') {
                // Coloca api que vai receber as informações de contexto
                post.Dados.push(json);
                postMethod(post);
            }
        });
    };

    var postMethod = function(json) {
        var client = new Client();

        var args = {
            data: json,
            headers: { "Content-Type": "application/json" }
        };

        client.post("http://datalivemarketing.com.br/IntegradormodeloR.Web/api/WebScraping", args,
            function(data, response) {
                res.send(data);
            });
    }

    url = 'http://elvisdeveloper.com/';
    sendAPI(url);
});

app.listen('8081')
console.log('Magic happens on port 8081');
exports = module.exports = app;